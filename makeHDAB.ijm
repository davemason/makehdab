//-- proof of concept script to convert a two channel fluorescent image into a pseudo HDAB image
//-- Written by Dave Mason (@dn_mason)

//-- RGB colour vectors for HDAB sourced from Fiji: [Image > Color > Color Deconvolution]
vecH=newArray(0.65,0.704,0.286);
vecD=newArray(0.27,0.57,0.78);

//-- close any open images
close("*");
//-- open the example tif with two channels 1=DAPI (nuclei) 2=PCK (tumour)
open("example.tif");
title=getTitle();
run("8-bit");

//-- Duplicate the images to make the RGB channels for Haematoxylin
setSlice(1);
run("Duplicate...", "title=R");
run("Duplicate...", "title=G");
run("Duplicate...", "title=B");

//-- Use the relevant fractions of each RGB to make a Haematoxylin channel
selectWindow("R");
run("Multiply...", "value="+vecH[0]);
selectWindow("G");
run("Multiply...", "value="+vecH[1]);
selectWindow("B");
run("Multiply...", "value="+vecH[2]);
run("Merge Channels...", "c1=R c2=G c3=B create ignore");
run("Invert");
rename("H");

//-- select the original image and create the DAB channel
selectWindow(title);
setSlice(2);
run("Duplicate...", "title=R");
run("Duplicate...", "title=G");
run("Duplicate...", "title=B");
//-- Haematoxylin channel
selectWindow("R");
run("Multiply...", "value="+vecD[0]);
selectWindow("G");
run("Multiply...", "value="+vecD[1]);
selectWindow("B");
run("Multiply...", "value="+vecD[2]);
run("Merge Channels...", "c1=R c2=G c3=B create ignore");
run("Invert");
rename("DAB");

//-- combine the images
imageCalculator("Average stack", "H","DAB");
run("RGB Color");
run("Enhance Contrast", "saturated=0");
rename("HDAB");
//-- tidy up
close("DAB");
close("H");
close(title);