Example script to convert a fluorescent image into a pseudo HDAB image in response to ["this thread on Image.sc"](https://forum.image.sc/t/converting-fluorescent-images-to-brightfield/40639) and a general curiosity about the process.

### Process ###
The original fluorescent image is assumed to contain DAPI in channel 1 and another marker in channel 2. These will be mapped to a pseudo Haematoxylin and DAB respectively. This is done by converting a single channel fluorescent image into it's component RGB vector matching either of the stains. The two channels are combined and contrasted.

No guarantees whatsoever are provided as to the validity or utilty of this script!